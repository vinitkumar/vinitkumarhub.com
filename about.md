---
layout: default
title: About
---
## About

<img src="https://cldup.com/jLL_Y6uO09-3000x3000.png" height="600" width="800">

Vinit is a Senior Engineer at <a href="http://www.socialschools.nl/">Socialschools B.V.</a> who likes writing code that scales well.

He has written a good amount of code in building a Social Network, Multi-tenant Content Management System and Websites and their architecture.

He is an active member of the Free & Open Source Software community and has contributed to many projects. He is an avid book reader and reads a lot of technical books for fun.

You can get in touch with Vinit by clicking on the links in the sidebar.


