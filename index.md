---
layout: default-index
title: About
---
<img src="https://cldup.com/iBiAh_VnyX-3000x3000.png" style="margin-top: 40px">

## About

Vinit is a Senior Engineer at Socialschools with expertise in writing scalable backend systems. He has written Python, Golang, JavaScript based backend systems and  also enjoys writing clean frontend using BackboneJS, AngularJS, ReactJS and even vanilla Javascript with ease.

At Socialschools, he has been working on Performance, scalability and security aspects as well as rapidly building new features that add value to the product. He has architected and built a Multi-Tenant CMS that has helped in pushing out features to multiple clients in less time and satisfy all their website requirements.

Vinit has also done lot of consulting work in Python/Django, JavaScript, Node.js and PHP in the past. You can get in touch with him using the links in the right sidebar.
